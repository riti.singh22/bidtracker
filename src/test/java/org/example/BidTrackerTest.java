package org.example;


import org.assertj.core.api.Assertions;
import org.example.exception.InvalidBidException;
import org.example.model.Bid;
import org.example.model.Item;
import org.example.model.User;
import org.example.service.BidTracker;
import org.example.service.BidTrackerImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

public class BidTrackerTest {

    private final User user1 = new User("u1", "Nicolas Test");
    private final User user2 = new User("u2", "Randolph Carter");
    private final User user3 = new User("u3", "Herbert West");
    private final Item item1 = new Item("i1", "item1", "Brilliant!");
    private final Item item2 = new Item("i2", "item2", "Brilliant!");
    private final Item item3 = new Item("i3", "item3", "Brilliant!");

    private BidTracker bidTracker;

    @Before
    public void init() {
        bidTracker = new BidTrackerImpl();
    }

    @Test
    public void recordUserBidOnItem_shouldAddAFirstBidToItem_whenBidIsValid() throws InvalidBidException {
        Bid bid = new Bid(user1, 10);

        bidTracker.recordUserBidOnItem(bid, item1);

        PriorityQueue<Bid> actualBidListOnItem1 = bidTracker.getCurrentAuctionBoardCopy().get(item1);
        PriorityQueue<Bid> expectedBidListOnItem1 = new PriorityQueue();
        expectedBidListOnItem1.add(bid);
        Assertions.assertThat(actualBidListOnItem1)
                .usingRecursiveComparison()
                .isEqualTo(expectedBidListOnItem1);
    }

    @Test
    public void recordUserBidOnItem_shouldAddSeveralBidsToItem_whenBidsAreValid() throws InvalidBidException {
        bidTracker.recordUserBidOnItem(new Bid(user1, 10), item1);
        bidTracker.recordUserBidOnItem(new Bid(user2, 20), item1);
        bidTracker.recordUserBidOnItem(new Bid(user1, 30), item1);

        PriorityQueue<Bid> actualBidsListOnItem1 = bidTracker.getCurrentAuctionBoardCopy().get(item1);
        PriorityQueue<Bid> expectedBidsListOnItem1 = new PriorityQueue();
        expectedBidsListOnItem1.add(new Bid(user1, 10));
        expectedBidsListOnItem1.add(new Bid(user2, 20));
        expectedBidsListOnItem1.add(new Bid(user1, 30));
        Assertions.assertThat(actualBidsListOnItem1)
                .usingRecursiveComparison()
                .isEqualTo(expectedBidsListOnItem1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void recordUserBidOnItem_shouldThrowIllegalArgumentException_whenItemIsNull() throws InvalidBidException {

        bidTracker.recordUserBidOnItem(new Bid(user1, 10), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void recordUserBidOnItem_shouldThrowIllegalArgumentException_whenBidIsNull() throws InvalidBidException {
        bidTracker.recordUserBidOnItem(null, item1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void recordUserBidOnItem_shouldThrowIllegalArgumentException_whenUserIsNull() throws InvalidBidException {
        bidTracker.recordUserBidOnItem(new Bid(null, 10), item1);
    }

    @Test(expected = InvalidBidException.class)
    public void recordUserBidOnItem_shouldThrowInvalidBidException_whenBidIsLowerThanCurrentlyWinningBid() throws InvalidBidException {
        bidTracker.recordUserBidOnItem(new Bid(user1, 10), item1);

        Bid lowBid = new Bid(user2, 5);
        bidTracker.recordUserBidOnItem(lowBid, item1);
    }

    @Test(expected = InvalidBidException.class)
    public void recordUserBidOnItem_shouldThrowInvalidBidException_whenBidIsSameAsCurrentlyWinningBid() throws InvalidBidException {
        bidTracker.recordUserBidOnItem(new Bid(user1, 10), item1);

        Bid sameBid = new Bid(user2, 10);
        bidTracker.recordUserBidOnItem(sameBid, item1);
    }

    @Test
    public void recordUserBidOnItem_shouldAddSeveralBidsToItem_whenSomeBidsAreInvalid() throws InvalidBidException {
        bidTracker.recordUserBidOnItem(new Bid(user1, 10), item1);
        bidTracker.recordUserBidOnItem(new Bid(user2, 20), item1);
        try { // invalid bid
            bidTracker.recordUserBidOnItem(new Bid(user3, 15), item1);
        } catch (InvalidBidException e) { /* Silencing the exception as it is irrelevant for this test */ }
        bidTracker.recordUserBidOnItem(new Bid(user1, 30), item1);

        PriorityQueue<Bid> bidsListOnItem1 = bidTracker.getCurrentAuctionBoardCopy().get(item1);

        PriorityQueue<Bid> expectedBidsOnItem1 = new PriorityQueue<>(Arrays.asList(
                new Bid(user1, 10),
                new Bid(user2, 20),
                new Bid(user1, 30)));
        Assertions.assertThat(bidsListOnItem1)
                .usingRecursiveComparison()
                .isEqualTo(expectedBidsOnItem1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getCurrentWinningBidForItem_shouldThrowIllegalArgumentException_whenItemIsNull() {
        bidTracker.getCurrentWinningBidForItem(null);
    }

    @Test
    public void getCurrentWinningBidForItem_shouldReturnEmptyOptional_whenItemHasNoBid() {
        Optional<Bid> bid = bidTracker.getCurrentWinningBidForItem(item1);
        assertEquals(Optional.empty(), bid);
    }

    @Test
    public void getCurrentWinningBidForItem_shouldReturnOptionalWithAValue_whenItemHasBids() throws InvalidBidException {
        bidTracker.recordUserBidOnItem(new Bid(user1, 10), item1);
        bidTracker.recordUserBidOnItem(new Bid(user2, 20), item1);

        Optional<Bid> bid = bidTracker.getCurrentWinningBidForItem(item1);

        assertTrue(bid.isPresent());
        assertEquals(bid.get(), new Bid(user2, 20));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getAllBidsForItem_shouldThrowIllegalArgumentException_whenItemIsNull() {

        bidTracker.getAllBidsForItem(null);
    }

    @Test
    public void getAllBidsForItem_shouldReturnEmptyList_whenItemHasNoBid() {
        PriorityQueue<Bid> bids = bidTracker.getAllBidsForItem(item1);
        assertTrue(bids.isEmpty());
    }

    @Test
    public void getAllBidsForItem_shouldReturnTheCorrectListOfBids_whenItemHasBids() throws InvalidBidException {
        bidTracker.recordUserBidOnItem(new Bid(user1, 10), item1);
        bidTracker.recordUserBidOnItem(new Bid(user2, 20), item1);

        PriorityQueue<Bid> actualBids = bidTracker.getAllBidsForItem(item1);

        PriorityQueue<Bid> expectedBids = new PriorityQueue<>(Arrays.asList(
                new Bid(user1, 10),
                new Bid(user2, 20)));
        Assertions.assertThat(actualBids)
                .usingRecursiveComparison()
                .isEqualTo(expectedBids);

    }


    @Test
    public void getAllItemsWithBidFromUser_shouldReturnEmptySet_whenUserIsNull() {
        Set<Item> items = bidTracker.getAllItemsWithBidFromUser(null);
        assertTrue(items.isEmpty());
    }

    @Test
    public void getAllItemsWithBidFromUser_shouldReturnEmptySet_whenUserHasNoBid() {
        Set<Item> items = bidTracker.getAllItemsWithBidFromUser(user1);
        assertTrue(items.isEmpty());
    }

    @Test
    public void getAllItemsWithBidFromUser_shouldReturnCorrectItemSet_whenUserHasBids() throws InvalidBidException {
        bidTracker.recordUserBidOnItem(new Bid(user1, 10), item1); // bid on item1
        bidTracker.recordUserBidOnItem(new Bid(user2, 20), item1);
        bidTracker.recordUserBidOnItem(new Bid(user1, 30), item1); // second bid on item1
        bidTracker.recordUserBidOnItem(new Bid(user2, 10), item2);
        bidTracker.recordUserBidOnItem(new Bid(user3, 20), item2);
        bidTracker.recordUserBidOnItem(new Bid(user3, 10), item3);
        bidTracker.recordUserBidOnItem(new Bid(user1, 20), item3); // bid on item3

        Set<Item> itemList = bidTracker.getAllItemsWithBidFromUser(user1);

        Set<Item> expectedItemList = new HashSet<>(Arrays.asList(item1, item3));
        assertEquals(expectedItemList, itemList);
    }
}