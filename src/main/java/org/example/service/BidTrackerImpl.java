package org.example.service;


import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

import org.example.model.User;
import org.example.model.Bid;
import org.example.model.Item;
import org.example.exception.InvalidBidException;

import static org.example.util.Helper.containsBidFromUser;
import static org.example.util.Helper.validateBidAndItem;


/**
 * An implementation of the {@link BidTracker} interface with an in-memory data storage.
 * <p>
 * Some limitations of the current implementation are:
 * - It stores the data about the auction in a ConcurrentHashMap. Other implementations could query a persistent
 * data storage (Database, flat files, ..)
 * - It assumes all the Users given as parameters are allowed to place a bid.
 * - It assumes all the Items given as parameters can be bid on.
 */
public class BidTrackerImpl implements BidTracker {

    private final Map<Item, PriorityQueue<Bid>> auctionBoard;
    private ReadWriteLock lock = new ReentrantReadWriteLock();

    public BidTrackerImpl() {
        auctionBoard = new ConcurrentHashMap<>(100, 0.75f);
    }

    /**
     * Getter for the auction board.
     *
     * @return a clone auctionBoard
     */
    @Override
    public Map<Item, PriorityQueue<Bid>> getCurrentAuctionBoardCopy() {
        Map<Item, PriorityQueue<Bid>> itemPriorityQueueHashMap = new HashMap<>();
        itemPriorityQueueHashMap.putAll(auctionBoard);
        return itemPriorityQueueHashMap;
    }


    @Override
    public void recordUserBidOnItem(Bid bid, Item item) throws InvalidBidException {
        validateBidAndItem(bid, item);
        try {
            recordUserBidOnItemSync(bid, item);
        } catch (InterruptedException e) {
            // TODO - LOGGER to be used in production code
            System.out.println("Thread interrupted: " + e);
        }
    }

    // ReadWrite Reentrant Lock done to ensure that only one bid is processed at a time.
    private void recordUserBidOnItemSync(Bid bid, Item item) throws InvalidBidException, InterruptedException {
        lock.writeLock().lockInterruptibly();
        checkBidIsHighEnough(bid, item);
        addBidOnItem(item, bid);
        lock.writeLock().unlock();
    }

    private void checkBidIsHighEnough(Bid bid, Item item) throws InvalidBidException {
        Optional<Bid> currentWinningBid = getCurrentWinningBidForItem(item);
        if (currentWinningBid.isPresent() && bid.getValue() <= currentWinningBid.get().getValue()) {
            throw new InvalidBidException(String.format(
                    "A bid of £%s on item %s is too low. It should be more than the current winning bid: £%s)",
                    bid.getValue(),
                    item,
                    currentWinningBid.get().getValue()));
        }
    }

    @Override
    public Optional<Bid> getCurrentWinningBidForItem(Item item) {
        return Optional.ofNullable(getAllBidsForItem(item).peek());
    }

    @Override
    public PriorityQueue<Bid> getAllBidsForItem(Item item) {
        if (Objects.isNull(item)) {
            throw new IllegalArgumentException();
        }
        return auctionBoard.getOrDefault(item, new PriorityQueue<>());
    }

    @Override
    public Set<Item> getAllItemsWithBidFromUser(User user) {
        return auctionBoard.entrySet().stream()
                .filter(entry -> containsBidFromUser(entry.getValue(), user))
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());
    }

    private void addBidOnItem(Item item, Bid bid) {
        PriorityQueue<Bid> bidsOnItem = auctionBoard.getOrDefault(item, new PriorityQueue<>());
        bidsOnItem.add(bid);
        auctionBoard.put(item, bidsOnItem);
    }
}