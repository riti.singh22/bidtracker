package org.example.service;

import org.example.model.User;
import org.example.model.Bid;
import org.example.model.Item;
import org.example.exception.InvalidBidException;

import java.util.*;

/**
 * The interface for a Bid Tracker.
 * This interface exposes methods allowing {@link User}s to post {@link Bid}s on {@link Item}s
 * and query the current state of the auction.
 */
public interface BidTracker {

    /**
     * Records a bid for a given item.
     *
     * @param bid  the bid to record
     * @param item the item to bid on
     * @throws InvalidBidException when the bid is invalid
     */
    void recordUserBidOnItem(Bid bid, Item item) throws InvalidBidException;

    /**
     * @param item the item
     * @return the current winning bid (last valid bid), as an {@link Optional}, for the given item
     */
    Optional<Bid> getCurrentWinningBidForItem(Item item);

    /**
     * @param item the item
     * @return the list of all bids made for the given item
     */
    PriorityQueue<Bid> getAllBidsForItem(Item item);

    /**
     * @param user the user to get the list of items for
     * @return the list of all items bid on by the given user
     */
    Set<Item> getAllItemsWithBidFromUser(User user);

    /**
     * Creates a copy of the auction board
     * @return the auction board
     */
    Map<Item, PriorityQueue<Bid>> getCurrentAuctionBoardCopy();

}