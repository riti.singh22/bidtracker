package org.example.util;

import org.example.model.Bid;
import org.example.model.Item;
import org.example.model.User;

import java.util.Objects;
import java.util.PriorityQueue;

public class Helper {

    public static boolean containsBidFromUser(PriorityQueue<Bid> bids, User user) {
        return bids.stream().anyMatch(bid -> bid.isFromUser(user));
    }

    public static void validateBidAndItem(Bid bid, Item item) {
        if (Objects.isNull(item))
            throw new IllegalArgumentException("Item can't be null");
        else if (Objects.isNull(bid))
            throw new IllegalArgumentException("Bid can't be null");
        else if (Objects.isNull(bid.getUser()))
            throw new IllegalArgumentException("Bid's user can't be null");
    }
}
