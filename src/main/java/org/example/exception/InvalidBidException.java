package org.example.exception;

/**
 * A Generic {@link Exception} thrown when trying to make an invalid bid.
 * Could be for multiple reasons: The bid is too low, the item is not in the auction anymore, etc..
 * The reason should be explained in the message or this Exception could be subclassed by finer grain Exceptions.
 */
public class InvalidBidException extends Exception {

  public InvalidBidException(String message) {
    super(message);
  }
}
